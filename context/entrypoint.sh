#!/bin/bash
if [ $# -eq 0 ]; then
   /usr/games/fortune | /usr/games/cowsay -f dragon
else
   /usr/games/cowsay -f tux "$@"
fi
